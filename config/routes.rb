Rails.application.routes.draw do
  resources :posts
  root 'posts#index'

  get :create, to: 'posts#new'
  post :create, to: 'posts#create'

  get :destroy, to: 'posts#destroy'

  
end
